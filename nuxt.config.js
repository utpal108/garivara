export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Garivara',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: '্রাত্যহিক জীবনে আরেকটু স্বাছন্দ্য যোগ করতে অ্যান্ডগাড়িভাড়া এখন আরো উন্নত ও সাশ্রয়ী। এক অ্যাপেই আধুনিক জীবনের সকল সুযোগ সুবিধা ও সমাধান।' },
      { name: 'robots', content: 'index, follow' },
      { name: 'googlebot', content: 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1' },
      { name: 'bingbot', content: 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1' },
      { property: 'og:locale', content: 'en_US' },
      { property: 'og:type', content: 'website' },
      { property: 'og:title', content: 'অ্যান্ডগাড়িভাড়া - দেশের ১ নম্বর ডিজিটাল প্ল্যাটফর্ম' },
      { property: 'og:description', content: '্রাত্যহিক জীবনে আরেকটু স্বাছন্দ্য যোগ করতে অ্যান্ডগাড়িভাড়া এখন আরো উন্নত ও সাশ্রয়ী। এক অ্যাপেই আধুনিক জীবনের সকল সুযোগ সুবিধা ও সমাধান।' },
      { property: 'og:url', content: 'https://andgarivara.com' },
      { property: 'og:site_name', content: 'অ্যান্ডগাড়িভাড়া' },
      { property: 'og:image', content: '/assets/img/logo.png' },
      { property: 'og:image:width', content: '1200' },
      { property: 'og:image:height', content: '630' },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:title', content: 'andgarivara' },
      { name: 'twitter:description', content: 'andgarivara' },
      { name: 'twitter:image', content: '/assets/img/logo.png' },
      { name: 'msapplication-TileImage', content: '/assets/img/logo.png' },
      { name: 'msapplication-TileColor', content: '#018ec9' },
      { name: 'theme-color', content: '#02449b' },
      { name: 'keywords', content: 'service, Car Sharing, Car Rental, Car Leasing, Share, Rental, Leasing' },
      { name: 'author', content: 'andgarivara' }
    ],
    link: [
      { rel: 'canonical', href: 'https://andgarivara.com' },
      { rel: 'stylesheet', type: 'text/css', href: '/assets/css/style.css' },
      { rel: 'icon', type: 'image/png', sizes:'16x16', href: '/assets/img/favicon-16x16.png' },
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/assets/img/favicon-16x16.png' },
      { rel: 'apple-touch-icon-precomposed', type: 'image/x-icon', href: '/assets/img/apple-icon-72x72.png', sizes:"72x72" },
      { rel: 'apple-touch-icon-precomposed', type: 'image/x-icon', href: '/assets/img/apple-icon-114x114.png', sizes:"114x114" },
      { rel: 'apple-touch-icon-precomposed', type: 'image/x-icon', href: '/assets/img/apple-icon-144x144.png', sizes:"144x144" },
      { rel: 'apple-touch-icon-precomposed', type: 'image/x-icon', href: '/assets/img/favicon-16x16.png' }
    ],
    script: [
      { src: 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js'},
      { src: 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js'},
      { src: '/assets/vendor/jquery/jquery.min.js', body: true},
      { src: '/assets/vendor/jquery/popper.min.js', body: true},
      { src: '/assets/vendor/bootstrap/js/bootstrap.min.js', body: true},
      { src: '/assets/js/venobox.min.js', body: true},
      { src: '/assets/js/wow.min.js', body: true},
      { src: '/assets/js/confetti.js', body: true},
      { src: '/assets/js/owl.carousel.min.js', body: true},
      { src: '/assets/vendor/datepicker/datepicker.min.js', body: true},
      { src: '/assets/js/jquery.meanmenu.js', body: true},
      { src: '/assets/js/custom.js', body: true},
    ]

  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy'
  ],
  proxy: {
    '/api': {
      target: process.env.API_BASE_URL,
      pathRewrite: {
        '^/api' : '/'
      }
    }
  },
  axios: {
    proxyHeaders: false,
    credentials: false
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
